//
//  PersonDB.swift
//  consis-ios
//
//  Created by Erick Cienfuegos on 6/22/17.
//  Copyright © 2017 er1ck. All rights reserved.
//

import Foundation
import SQLite

class ContactDB {
    
    fileprivate let contacts = Table("contacts")
    fileprivate let id = Expression<Int64>("id")
    
    private let name = Expression<String?>("name")
    private let lastname = Expression<String>("lastname")
    private let direction = Expression<String>("direction")
    private let tel = Expression<String>("tel")
    private let email = Expression<String>("email")
    private let birthday = Expression<String>("birthday")
    private let state = Expression<String>("state")
    private let nickname = Expression<String>("nickname")
    private let password = Expression<String>("password")
    
    static let instance = ContactDB()
    fileprivate let db: Connection?
    
    fileprivate init() {
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
            ).first!
        
        do {
            db = try Connection("\(path)/Stephencelis.sqlite3")
            createTable()
        } catch {
            db = nil
            print ("Unable to open database")
        }
    }
    
    func createTable() {
        do {
            try db!.run(contacts.create(ifNotExists: true) { table in
                table.column(id, primaryKey: true)
                table.column(name)
                table.column(lastname)
                table.column(direction)
                table.column(tel)
                table.column(email)
                table.column(birthday)
                table.column(state)
                table.column(nickname/*, unique: true*/)
                table.column(password)
            })
        } catch {
            print("Unable to create table")
        }
    }
    
    func addContact(_ cname: String, clastname: String, cdirection: String, ctel: String, cemail: String, cbirthday: String, cstate: String, cnickname: String, cpassword: String) -> Int64? {
        do {
            let insert = contacts.insert(name <- cname, lastname <- clastname, direction <- cdirection, tel <- ctel ,email <-  cemail,birthday <- cbirthday, state <- cstate, nickname <- cnickname , password <- cpassword)
            let id = try db!.run(insert)
            print(["id": id])
            return id
        } catch {
            print("Insert failed")
            return nil
        }
    }
    
    func addDummy() -> Int64? {
        do {
            let insert = contacts.insert(name <- "erick", lastname <- "cienfuegos", direction <- "siempre viva", tel <- "ctel" ,email <-  "cemail",birthday <- "cbirthday", state <- "cstate", nickname <- "cnickname" , password <- "cpassword")
            let id = try db!.run(insert)
            print(["id": id])
            return id
        } catch {
            print("Insert failed")
            return nil
        }
    }
    
    func getContacts() -> [Contact] {
        var contacts = [Contact]()
        
        do {
            for contact in try db!.prepare(self.contacts) {
                contacts.append(Contact(
                    id: contact[id],
                    name: contact[name]!,
                    lastname: contact[lastname],
                    direction: contact[direction],
                    tel: contact[tel],
                    email: contact[email],
                    birthday: contact[birthday],
                    state: contact[state],
                    nickname: contact[nickname],
                    password: contact[password]))
            }
        } catch {
            print("Select failed")
        }
        
        return contacts
    }
    
    }

