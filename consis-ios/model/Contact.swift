//
//  Person.swift
//  consis-ios
//
//  Created by Erick Cienfuegos on 6/22/17.
//  Copyright © 2017 er1ck. All rights reserved.
//

import Foundation



class Contact {
    
    var id: Int64?
    var name: String?
    var lastname: String?
    var direction: String?
    var tel: String?
    var email: String?
    var birthday: String?
    var state: String?
    var nickname: String?
    var password: String?
    
    init(id: Int64) {
        self.id = id
        
    }
    
    init(id: Int64, name: String, lastname: String, direction: String, tel: String, email: String, birthday: String, state: String, nickname: String, password: String) {
        
        self.id = id
        self.name = name
        self.lastname = lastname
        self.direction = direction
        self.tel = tel
        self.email = email
        self.birthday = birthday
        self.state = state
        self.nickname = nickname
        self.password = password
    }
}
