//
//  FormViewController.swift
//  consis-ios
//
//  Created by aplicacionesmoviles on 21/06/17.
//  Copyright © 2017 er1ck. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {

    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var lblLastname: UITextField!
    @IBOutlet weak var lblDirection: UITextField!
    @IBOutlet weak var lblTel: UITextField!
    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblBirthday: UITextField!
    @IBOutlet weak var lblState: UITextField!
    @IBOutlet weak var lblNick: UITextField!
    @IBOutlet weak var lblPasswd: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goSave(_ sender: Any) {
        print("save")
        
        let name = lblName.text
        let lastname =  lblLastname.text
        let direction =  lblDirection.text
        let tel =  lblTel.text
        let email =  lblEmail.text
        let bir =  lblBirthday.text
        let state =  lblState.text
        let nick =  lblNick.text
        let passwd =  lblPasswd.text

        
        if let id = ContactDB.instance.addContact(name!, clastname: lastname!, cdirection: direction!, ctel: tel!, cemail: email!, cbirthday: bir!, cstate: state!, cnickname: nick!, cpassword: passwd!) {
            print("Contact \(name!) add with ID: \(id)" )
                self.alert(message: "Save Contact \(name!) with id: \(id.description )")
            }

    }

    @IBAction func saveDummy(_ sender: Any) {
        if let id = ContactDB.instance.addDummy(){
            self.alert(message: "Dummy Contact Create with id: \(id.description)",title: "Dummy")
        }
    }


    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
