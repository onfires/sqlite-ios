//
//  DetailViewController.swift
//  consis-ios
//
//  Created by Erick Cienfuegos on 6/22/17.
//  Copyright © 2017 er1ck. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var contact: Contact!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var lastname: UILabel!
    @IBOutlet weak var direction: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var password: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.title = contact.name
        
        name.text = contact.name
        lastname.text = contact.lastname
        direction.text = contact.direction
        phone.text = contact.tel
        email.text = contact.email
        birthday.text = contact.birthday
        state.text = contact.state
        nickname.text = contact.nickname
        
        password.text = contact.password

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
