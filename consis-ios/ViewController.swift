//
//  ViewController.swift
//  consis-ios
//
//  Created by aplicacionesmoviles on 20/06/17.
//  Copyright © 2017 er1ck. All rights reserved.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let animationView = LOTAnimationView(name: "E.json") {
            animationView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
            animationView.center = self.view.center
            animationView.contentMode = .scaleAspectFill
            
            view.addSubview(animationView)
            
            animationView.play()
        }
        
        self.navigationController?.title = "Consis"
        
        
    }

    @IBAction func goList(_ sender: Any) {

        //let vc:ListTableViewController = ListTableViewController()
        
    
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "listViewID") as! ListTableViewController;
        
        self.navigationController!.pushViewController(vc, animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIViewController {
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
